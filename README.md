# Docker Compose - Production Tools
This repository contains the ```docker-compose.yml``` files for the Python Django application and its corresponding monitoring tools.

## Service - Application
The Production Application Services are split into 3. (Located at ```services-app```)

### MySQL
**Docker Hub Link:** https://hub.docker.com/_/mysql/

**Repository Link:** N/A

A MySQL database is used in both development and production environments.

| Property      | Value          |
| -------------: |:-------------: |
| Base Image    | ```mysql:5.7``` |
| Container Name| mysql-djangodb |
| Volume        | /var/lib/mysql |   
| Port Exposure | 3306           |

### Python - Django
**Docker Hub Link:** https://hub.docker.com/r/thebean/django-frontend/

**Repository Link:** https://bitbucket.org/thebeanunchained/the-bean-unchained

This is a Python based container with the repository's ```requirements.txt``` dependencies met. In development mode the Django service is run using the built in ```manage.py runserver``` however in production, the ```gunicorn``` pip module is used.

| Property      | Value          |
| -------------: |:-------------: |
| Base Image    | ```debain:stretch-slim``` |
| Container Name| django-frontend |
| Volume        | None |   
| Port Exposure | 8080 |
| Port Mapping  | ```8080:8080``` |

### Nginx
**Docker Hub Link:** https://hub.docker.com/r/thebean/nginx-ldap/ 

**Repository Link:** https://bitbucket.org/thebeanunchained/nginx-ldap

NGINX is used as the reverse proxy of choice. The main root of the web application is accessible publically, however access to metrics analysis and admin user management is managed via an LDAP server.

| Property      | Value          |
| -------------: |:-------------: |
| Base Image    | ```debain:stretch-slim```      |
| Container Name| django-frontend |
| Volume        | None |   
| Port Exposure | 8080           |
| Port Mapping  | ```8080:8080```      |

## Service - Monitoring
The monitoring services are used to collect metrics about the running containers. (Located at ```services-monitoring```)

## Service - HashiCorp Vault

## Other Important Links
Any other project important links are shown below.

| Repository       | Description | Link |
| :----------------|:------------|:-----|
|Production Docker Compose | (This Repo) Holds the docker-compose used for production | https://bitbucket.org/thebeanunchained/docker-compose-djangoapp |
|Production Source Code | Contains the application source code. It is written using the Django framework. | https://bitbucket.org/thebeanunchained/the-bean-unchained |